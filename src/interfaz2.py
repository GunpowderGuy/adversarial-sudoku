import time
import readline
import sys
import logica

from colorama import init
init(strip=not sys.stdout.isatty()) # strip colors if stdout is redirected
from termcolor import cprint
from pyfiglet import figlet_format

cprint(figlet_format('sudoku!', font='starwars'),
       'yellow', 'on_blue', attrs=['bold'])


def interfaz(N,tablero):
    print("\033[31;1;4m")
    readline.set_startup_hook(lambda: readline.insert_text(logica.cadena(tablero)))
    res = input('Edit this: \n')
    return logica.matrizdeints(res)


def ingresar(N,matriz):
    board = interfaz(N,matriz)

    if logica.rellenado(board,matriz):
         return board
    else :
         print("el tablero del usuario tiene numeros cambiados")
         exit(1)


def resultado(booleano):
    if booleano:
        return "gano"
    else:
        return "perdió"

def main():
    nombre = input("ingrese nombre : ")
    comienzo = time.time()
    N = int(input("ingrese N : \n\n"))

    matriz = logica.matrix(N)
    matriz_usuario = ingresar(N,matriz)

    gano = resultado(logica.sudoku(matriz_usuario,N))
    print("\n"+gano, sep ="\n")

    score = time.time() - comienzo

    archivo = open("puntajes.txt", 'a', encoding='utf-8')
    archivo.write("\n"+nombre+" "+gano+" "+str(round(score,2))+" segundos "+"\n")


main()
