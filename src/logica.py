from functools import reduce
import operator as op
import numpy as np

def listaIgualset(lista):
    return len(lista) == len(set(lista))
# esta funcion chequea que una lista no contenga el


def check_blocks(Ogboard):
    board = np.array(Ogboard)

    size_of_blocks = int(np.sqrt(board.shape[1]))
    indices = np.arange(0,size_of_blocks)

    is_permutation = True
    for x in range(0,size_of_blocks):
        for y in range(0,size_of_blocks):
            rid = indices + y*size_of_blocks
            cid = indices + x*size_of_blocks
            sub_block = board[np.ix_(rid,cid)]
            flat_block = sub_block.flatten()
            is_permutation = listaIgualset(flat_block)

            if not is_permutation:
                return is_permutation
    return is_permutation


def sudoku(board,N):
     def checkFlat(lista) :
         return reduce( op.and_, map( listaIgualset , lista)) # esta funcion aplica la funcion anteriormente explicada a una lista de listas (matriz bidimensional)
     def sliceVertical(board,columna):
         return list(map(lambda x : x[columna] ,board))

     horizontal = checkFlat(board) # se chequea que cada fila no contenga elementos repetidos
     flippedBoard = list(map(lambda x : sliceVertical(board,x),range(N*N))) # las columnas son convertidas en filas para que se pueda comprobar si hay elementos repetidos
     vertical = checkFlat(flippedBoard)
     return vertical and horizontal and check_blocks(board)


def matrix(N):
    dos = [#
    [0,3,0,2],
    [1,0,3,0],
    [0,2,1,0],
    [3,1,0,4],
    ]

    oldtres = [
    [None,None,None,None,None,None,None,None,None],
    [None,1,2,None,3,4,5,6,7],
    [None,3,4,5,None,6,1,8,2],
    [None,None,1,None,5,8,2,None,6],
    [None,None,8,6,None,None,None,None,1],
    [None,2,None,None,None,7,None,5,None],
    [None,None,3,7,None,5,None,2,8],
    [None,8,None,None,6,None,7,None,None],
    [2,None,7,None,8,3,6,1,5]
     ]


    tres = [
    [5,3,0,0,7,0,0,0,0],
    [6,0,0,1,9,5,0,0,0],
    [0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],
    [4,0,0,8,0,3,0,0,1],
    [7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],
    [0,0,0,4,1,9,0,0,5],
    [0,0,0,0,8,0,0,7,9],
]

    cuatro = [0,6,0,0,0,0,0,8,11,0,0,15,14,0,0,16],
    [15,11,0,0,0,16,14,0,0,0,12,0,0,6,0,0],
    [13,0,9,12,0,0,0,0,3,16,14,0,15,11,10,0],
    [2,0,16,0,11,0,15,10,1,0,0,0,0,0,0,0],
    [0,15,11,10,0,0,16,2,13,8,9,12,0,0,0,0],
    [12,13,0,0,4,1,5,6,2,3,0,0,0,0,11,10],
    [5,0,6,1,12,0,9,0,15,11,10,7,16,0,0,3],
    [0,2,0,0,0,10,0,11,6,0,5,0,0,13,0,9],
    [10,7,15,11,16,0,0,0,12,13,0,0,0,0,0,6],
    [9,0,0,0,0,0,1,0,0,2,0,16,10,0,0,11],
    [1,0,4,6,9,13,0,0,7,0,11,0,3,16,0,0],
    [16,14,0,0,7,0,10,15,4,6,1,0,0,0,13,8],
    [11,10,0,15,0,0,0,16,9,12,13,0,0,1,5,4],
    [0,0,12,0,1,4,6,0,16,0,0,0,11,10,0,0],
    [0,0,5,0,8,12,13,0,10,0,0,11,2,0,0,14],
    [3,16,0,0,10,0,0,7,0,0,6,0,0,0,12,0],

    if N == 2 :
        return dos
    if N == 3 :
        return tres
    if N == 4 :
        return cuatro


def cadena(tablero):
    return '\n'.join(','.join('%0.3f' % x for x in y) for y in tablero)


def matrizdeints(cadena):
    def linea(x):
        return list(map(int, x.split(",")))

    matrizStrings =  cadena.replace(" ","").replace(".","").replace("1","1").split("\n")
    return list(map(linea,matrizStrings))


def rellenado(matrizUsuario,matrizSudoku) :
    flat = lambda lista : reduce(op.add,lista)

    ms = flat(matrizSudoku)
    mu = flat(matrizUsuario)

    return reduce(op.add,map(lambda x : x[0] == None or x[0] == x[1] , zip(ms,mu)))
